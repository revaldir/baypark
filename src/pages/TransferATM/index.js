import React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {Gap, Header, ListAtm} from '../../components';
import {colors} from '../../utils';

const TransferATM = ({navigation}) => {
  return (
    <View style={styles.page}>
      <Header title="Transfer ATM" onPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
          <Gap height={20} />
          <ListAtm name="BCA" code="4213" />
          <Gap height={20} />
          <ListAtm name="BRI" code="4853" />
          <Gap height={20} />
          <ListAtm name="BNI" code="10754" />
          <Gap height={20} />
          <ListAtm name="Mandiri" code="59002" />
          <Gap height={20} />
          <ListAtm name="ATM Bersama" code="10394" />
        </View>
      </ScrollView>
    </View>
  );
};

export default TransferATM;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  content: {padding: 30},
});
