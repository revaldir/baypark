import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {ILTopUp} from '../../assets';
import {Gap, Header, Transaction} from '../../components';
import {colors} from '../../utils';

const ChooseMethod = ({navigation}) => {
  return (
    <View style={styles.page}>
      <Header title="Choose Method" onPress={() => navigation.goBack()} />
      <View style={styles.content}>
        <View style={styles.imgWrapper}>
          <Image source={ILTopUp} />
        </View>
        <Transaction
          desc="Instant Top Up"
          clickable
          onPress={() => navigation.replace('InstantTopUp')}
        />
        <Gap height={30} />
        <Transaction
          desc="Transfer ATM"
          clickable
          onPress={() => navigation.replace('TransferATM')}
        />
      </View>
    </View>
  );
};

export default ChooseMethod;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  content: {padding: 30},
  imgWrapper: {alignItems: 'center', marginVertical: 40},
});
