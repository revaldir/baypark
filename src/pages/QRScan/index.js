import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Button, Header} from '../../components';
import {colors} from '../../utils';
import QRCodeScanner from 'react-native-qrcode-scanner';

const QRScan = ({navigation}) => {
  return (
    <View style={styles.page}>
      <Header
        title="QR Payment"
        type="dark"
        onPress={() => navigation.goBack()}
        isQR
      />
      <QRCodeScanner
        showMarker={true}
        markerStyle={{borderColor: 'green', borderRadius: 10}}
      />
      <Button
        title="NEXT"
        type="Secondary"
        onPress={() => navigation.replace('TransactionDetail')}
      />
    </View>
  );
};

export default QRScan;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  qrWrapper: {height: 10, width: 10},
});
