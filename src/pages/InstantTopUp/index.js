/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import {ILTopUp} from '../../assets';
import {Button, Gap, Header, Input} from '../../components';
import {colors, fonts} from '../../utils';
import {Picker} from '@react-native-picker/picker';

const InstantTopUp = ({navigation}) => {
  const [bank, setBank] = useState('');
  return (
    <View style={styles.page}>
      <ScrollView>
        <Header
          title="Top Up"
          type="dark"
          onPress={() => navigation.goBack()}
        />
        <Gap height={40} />
        <View style={styles.imageWrapper}>
          <Image source={ILTopUp} style={styles.image} />
        </View>
        <Gap height={50} />
        <View style={styles.content}>
          <Input placeholder="Nominal Top Up" />
          <Gap height={16} />
          <View style={styles.pickerWrapper}>
            <Picker
              selectedValue={bank}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => setBank(itemValue)}>
              <Picker.Item label="Bank BCA" value="bbca" />
              <Picker.Item label="Bank Mandiri" value="bmandiri" />
              <Picker.Item label="Bank BRI" value="bbri" />
              <Picker.Item label="Bank BNI" value="bbni" />
            </Picker>
          </View>
          <Gap height={16} />
          <Input placeholder="Nomor Kartu" />
          <Gap height={40} />
          <Button
            title="SUBMIT"
            onPress={() => navigation.replace('TopUpSuccess')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default InstantTopUp;

const styles = StyleSheet.create({
  page: {backgroundColor: colors.white, flex: 1},
  imageWrapper: {alignItems: 'center'},
  image: {height: 160, width: 160},
  content: {paddingHorizontal: 40},
  pickerWrapper: {borderColor: colors.border, borderWidth: 1, borderRadius: 10},
  picker: {
    color: colors.text.secondary,
    fontFamily: fonts.primary[200],
  },
});
