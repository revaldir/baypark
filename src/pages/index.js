/* eslint-disable prettier/prettier */
import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import History from './History';
import Profile from './Profile';
import InstantTopUp from './InstantTopUp';
import TopUpSuccess from './TopUpSuccess';
import QRScan from './QRScan';
import QRSuccess from './QRSuccess';
import Withdraw from './Withdraw';
import WithdrawSuccess from './WithdrawSuccess';
import Vehicle from './Vehicle';
import Welcome from './Welcome';
import EditProfile from './EditProfile';
import ChooseVehicle from './ChooseVehicle';
import TransactionDetail from './TransactionDetail';
import HistoryDetail from './HistoryDetail';
import AddVehicle from './AddVehicle';
import ChooseMethod from './ChooseMethod';
import EditVehicle from './EditVehicle';
import TransferATM from './TransferATM';

export {
  Splash,
  Login,
  Register,
  Home,
  History,
  Profile,
  InstantTopUp,
  TopUpSuccess,
  QRScan,
  QRSuccess,
  Withdraw,
  WithdrawSuccess,
  Vehicle,
  Welcome,
  EditProfile,
  ChooseVehicle,
  TransactionDetail,
  HistoryDetail,
  AddVehicle,
  EditVehicle,
  ChooseMethod,
  TransferATM,
};
