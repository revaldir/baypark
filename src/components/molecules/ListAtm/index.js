import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const ListAtm = ({name, code}) => {
  return (
    <View>
      <View style={styles.atmWrapper}>
        <Text style={styles.textWrapper}>{name}</Text>
      </View>
      <View style={styles.virtWrapper}>
        <Text style={styles.textVirtual}>Virtual Account :</Text>
        <View style={styles.numWrapper}>
          <Text style={styles.txtCode}>'{code}' + Nomor Handphone</Text>
        </View>
      </View>
    </View>
  );
};

export default ListAtm;

const styles = StyleSheet.create({
  container: {flexDirection: 'row'},
  atmWrapper: {
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    elevation: 1,
    paddingLeft: 32,
    paddingVertical: 12,
  },
  textWrapper: {fontFamily: fonts.primary[600], fontSize: 14},
  virtWrapper: {backgroundColor: colors.secondary, flex: 1, marginRight: 20},
  textVirtual: {
    fontFamily: fonts.primary[500],
    fontSize: 12,
    paddingLeft: 32,
    paddingTop: 16,
  },
  numWrapper: {alignItems: 'center'},
  txtCode: {fontFamily: fonts.primary[600], fontSize: 12, paddingBottom: 16},
});
