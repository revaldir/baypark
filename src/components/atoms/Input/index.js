import React, {useState} from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {colors} from '../../../utils';

const Input = ({placeholder, secureTextEntry, value, disable}) => {
  const [border, setBorder] = useState(colors.border);

  const onFocusForm = () => {
    setBorder(colors.primary);
  };

  const onBlurForm = () => {
    setBorder(colors.border);
  };

  return (
    <View>
      <TextInput
        style={styles.input(border)}
        placeholder={placeholder}
        secureTextEntry={secureTextEntry}
        value={value}
        onFocus={onFocusForm}
        onBlur={onBlurForm}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: (border) => ({
    borderRadius: 10,
    padding: 12,
    borderColor: border,
    borderWidth: 1,
  }),
});
